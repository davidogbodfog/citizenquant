# -------------------------------------------------------------
# README.txt
# CitizenQuant
# Copyright (c) David Cohen, 2015
# david.cohen@sv.cmu.edu
# This file is part of CitizenQuant

# CitizenQuant is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CitizenQuant is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CitizenQuant.  If not, see <http://www.gnu.org/licenses/>.
# -------------------------------------------------------------

README last updated on Apr 12, 2015

