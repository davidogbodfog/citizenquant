package net.cohend.modelling;

import java.util.Map;

/**
 * Created by cohend on 4/12/15.
 */
public class Simulator {
    public static final int SIMULATOR_PERIOD = 1; // unit unspecified, implicit in domain model?
    public int time = 0;
    public Map<String, SimulatorObject> worldModel;

    public Simulator(Map<String, SimulatorObject> worldModel) {
        this.worldModel = worldModel;
    }

    /*
    * Increment time while taking the actions specified by the provided policy
    * */
    public void tick(Policy policy) {
        // execute the policy
        policy.takeAction(this);

        // Update every object in the world model according to the modeled dynamics
        for (SimulatorObject simulatorObject : worldModel.values()){
            simulatorObject.updateObjectModel(this);
        }

        time += SIMULATOR_PERIOD;
    }

}
