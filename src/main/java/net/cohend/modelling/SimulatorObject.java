package net.cohend.modelling;

/**
 * Created by cohend on 4/13/15.
 */
public abstract class SimulatorObject {
    public abstract void updateObjectModel(Simulator simulator);
}
