package net.cohend.modelling;

/**
 * Created by cohend on 4/12/15.
 */
public interface Policy {
    void takeAction(Simulator simulator);
    void prettyPrintPolicyParameters();
}
