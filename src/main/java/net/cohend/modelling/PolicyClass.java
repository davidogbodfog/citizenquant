package net.cohend.modelling;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

/**
 * A class which describes a parameterizable set of policies
 */
public interface PolicyClass {
    Map<Integer, Pair<Double, Double>> variableRangeMap();
    Policy newPolicy(double[] parameters);
}
