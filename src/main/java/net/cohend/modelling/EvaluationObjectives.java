package net.cohend.modelling;

/**
 * Created by cohend on 4/13/15.
 */
public interface EvaluationObjectives {
    int numObjectives();
    double[] evaluatePolicy(Policy policy, boolean verbose, boolean plot);
}
