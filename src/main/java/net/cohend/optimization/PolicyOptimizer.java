package net.cohend.optimization;

import net.cohend.misc.ParetoFrontPlot;
import net.cohend.modelling.EvaluationObjectives;
import net.cohend.modelling.Policy;
import net.cohend.modelling.PolicyClass;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cohend on 4/13/15.
 */
public class PolicyOptimizer {
    static PolicyClass policyClass;
    static EvaluationObjectives evaluationObjectives;

    public PolicyOptimizer(PolicyClass policyClass, EvaluationObjectives evaluationObjectives) {
        this.policyClass=policyClass;
        this.evaluationObjectives=evaluationObjectives;

        NondominatedPopulation result = new Executor()
                .withProblemClass(PolicyOptimizationProblem.class)
                .withAlgorithm("GDE3")
                .withMaxEvaluations(10000)
                .distributeOnAllCores()
                .run();

        System.out.println("number of non-dominated results:" + result.size());

        // display the results
        System.out.println("non-dominated results:");
        for (Solution solution : result) {
            this.policyClass.newPolicy(EncodingUtils.getReal(solution)).prettyPrintPolicyParameters();
            System.out.println(Arrays.toString(solution.getObjectives()));

            this.evaluationObjectives.evaluatePolicy(this.policyClass.newPolicy(EncodingUtils.getReal(solution)), true, true);
        }

        // plot the pareto front
        System.out.println("Printing pareto front");
        List<Number> x = new LinkedList<>();
        List<Number> y = new LinkedList<>();
        for (Solution solution : result) {
            x.add(-1 * solution.getObjectives()[0]);
            y.add(-1 * solution.getObjectives()[1]);
        }
        new ParetoFrontPlot("Pareto Front Plot", x, y);


    }


    public static class PolicyOptimizationProblem extends AbstractProblem {

        public PolicyOptimizationProblem() {
            super(policyClass.variableRangeMap().size(), evaluationObjectives.numObjectives());
        }

        @Override
        public void evaluate(Solution solution) {
            double[] x = EncodingUtils.getReal(solution);
            Policy p = policyClass.newPolicy(x);
            //TODO: vary model parameters, maybe this should happen within EvaluationObjectives
            solution.setObjectives(evaluationObjectives.evaluatePolicy(p, false, false));
        }

        @Override
        public Solution newSolution() {
            Solution solution = new Solution(getNumberOfVariables(),
                    getNumberOfObjectives());

            for (int i = 0; i < getNumberOfVariables(); i++) {
                solution.setVariable(i, new RealVariable(
                        policyClass.variableRangeMap().get(i).getLeft(),
                        policyClass.variableRangeMap().get(i).getRight()));
            }

            return solution;
        }
    }
}
