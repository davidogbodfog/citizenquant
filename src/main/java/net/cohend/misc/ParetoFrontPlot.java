package net.cohend.misc;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.util.List;

/**
 * Created by cohend on 4/22/15.
 */
public class ParetoFrontPlot extends ApplicationFrame {
    static String applicationTitle = "Pareto Front Plot";

    public ParetoFrontPlot(String chartTitle, List<Number> x, List<Number> y) {
        super(applicationTitle);
        JFreeChart lineChart = ChartFactory.createScatterPlot(
                chartTitle, "x", "y", createDataset(x, y), PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel( lineChart );
        chartPanel.setPreferredSize( new java.awt.Dimension( 640 , 480) );
        setContentPane(chartPanel);

        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }



    private XYDataset createDataset(List<Number> x, List<Number> y)
    {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries tmp = new XYSeries("x vs. y");
        for (int i = 0; i < x.size(); i++) {
            tmp.add(x.get(i), y.get(i));
        }
        dataset.addSeries(tmp);
        return dataset;
    }
}
