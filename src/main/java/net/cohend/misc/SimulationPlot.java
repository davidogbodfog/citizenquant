package net.cohend.misc;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.util.List;
import java.util.Map;

/**
 * Created by cohend on 4/22/15.
 */
public class SimulationPlot extends ApplicationFrame {
    static String applicationTitle = "CitizenQuant Simulation Plot";

    public SimulationPlot(String chartTitle, Map<String, ImmutablePair<List<Number>, List<Number>>> data) {
        super(applicationTitle);
        JFreeChart lineChart = ChartFactory.createXYLineChart(
                chartTitle, "x", "y", createDataset(data), PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel( lineChart );
        chartPanel.setPreferredSize( new java.awt.Dimension( 640 , 480) );
        setContentPane(chartPanel);

        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }



    private XYDataset createDataset(Map<String, ImmutablePair<List<Number>, List<Number>>> data)
    {
        XYSeriesCollection dataset = new XYSeriesCollection();

        for (String key : data.keySet()){
            XYSeries tmp = new XYSeries(key);
            List<Number> x = data.get(key).getLeft();
            List<Number> y = data.get(key).getRight();
            for (int i = 0; i <x.size(); i++) {
                tmp.add(x.get(i), y.get(i));
            }
            dataset.addSeries(tmp);
        }

        return dataset;
    }
}
