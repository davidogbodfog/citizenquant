package net.cohend.business_planner;

/**
 * Created by cohend on 7/14/15.
 */
public class BusinessParameters {
//    static final double marketerMonthlySalary = 3000;
//    static final double customerValuePerMonth = 200;
    static final double purchasedAdSpaceRetention = .5;
//    static final double customersAcquiredPerMarketerMonth = 30;
    static final double impressionsPurchasedPerAdvertisingDollar = 10;
    static final double userCostPerMonth = 3;
    static final double userMonthlyRetention = .5;
    static final double usersPerFacebookAdvertisingDollar = .15;
    static final double initialInvestment = 10000;
    static final double meanSwipesPerUserDay = 50;
    static final double fractionOfPaidSwipes = .5;
    static final double pricePerImpression = .1;
}
