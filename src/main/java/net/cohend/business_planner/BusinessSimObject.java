package net.cohend.business_planner;

import net.cohend.modelling.Simulator;
import net.cohend.modelling.SimulatorObject;

/**
 * Created by cohend on 4/14/15.
 */
public class BusinessSimObject extends SimulatorObject {

    int numUsers;
    int adsPurchased;
//    int numCustomers;
//    int numMarketers;
    int googleAdBudget; // assume we use Google to acquire customers
    int facebookAdBudget; // assume we use facebook to acquire users
    int adCapacity;

    double balance;
    double investment;
    boolean hasDefaulted = false;

    public BusinessSimObject(double initialInvestment) {
        numUsers = 0;
        adsPurchased = 0;
        googleAdBudget = 0;
        facebookAdBudget = 0;
        balance = initialInvestment;
        investment = initialInvestment;
    }

    @Override
    public void updateObjectModel(Simulator simulator) {
        // compute effects of attrition
        numUsers *= BusinessParameters.userMonthlyRetention;
//        numCustomers *= customerRetentionThisMonth();

        // compute effects of employee effort
        numUsers += facebookAdBudget * BusinessParameters.usersPerFacebookAdvertisingDollar;
        adCapacity = (int) (BusinessParameters.meanSwipesPerUserDay * BusinessParameters.fractionOfPaidSwipes * 30 * numUsers);

        adsPurchased = (int) (BusinessParameters.purchasedAdSpaceRetention *adsPurchased +
                googleAdBudget * BusinessParameters.impressionsPurchasedPerAdvertisingDollar);
        if (adsPurchased > adCapacity)
            hasDefaulted = true;

        // operating costs
        balance -= numUsers * BusinessParameters.userCostPerMonth;
        balance -= googleAdBudget;
        balance -= facebookAdBudget;

        // revenue
        balance += adsPurchased * BusinessParameters.pricePerImpression;

        if (balance<0)
            hasDefaulted = true;
    }
}
