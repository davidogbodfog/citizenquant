package net.cohend.business_planner;

import net.cohend.modelling.Policy;
import net.cohend.modelling.PolicyClass;
import net.cohend.modelling.Simulator;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;

/*
* For each month, we decide what delta in money
* we are going to spend on each category of expenditures compared to the previous month.
* Bottom-out at zero,
* */
public class BusinessPolicyClass implements PolicyClass{
    static Map<Integer, Pair<Double, Double>> variables = new HashMap<>();
    static {
        for (int i = 0; i < 6*2; i++) {
            variables.put(i, new ImmutablePair<>(-1000.0, 1000.0));
        }
    }

    @Override
    public Map<Integer, Pair<Double, Double>> variableRangeMap() {
        return variables;
    }

    @Override
    public Policy newPolicy(double[] parameters) {
        return new BusinessPolicy(parameters);
    }


    public class BusinessPolicy implements Policy{
        double[] deltaParameters;

        public BusinessPolicy(double[] deltaParameters) {
            this.deltaParameters = deltaParameters;
        }

        /*
        * Taking an action:
        * find the two policy parameters which describe the current month.
        * Add or subtract to the company's payroll according to these parameters
        * */
        @Override
        public void takeAction(Simulator simulator) {
            int month = simulator.time;
            BusinessSimObject businessSimObject = (BusinessSimObject) simulator.worldModel.get("business");
            businessSimObject.facebookAdBudget = Integer.max(0, businessSimObject.facebookAdBudget + (int)(Math.floor(deltaParameters[2*month])));
            businessSimObject.googleAdBudget = Integer.max(0, businessSimObject.googleAdBudget + (int)(Math.floor(deltaParameters[2*month + 1])));
        }

        @Override
        public void prettyPrintPolicyParameters() {
            System.out.println("Business Policy:");
            for (int i = 0; i < deltaParameters.length / 2; i++) {
                System.out.println("Month "+i+":");
                System.out.println("Change in facebok ad dollars:" + deltaParameters[2*i]);
                System.out.println("Change in google ad dollars:" + deltaParameters[2*i + 1]);
            }
        }
    }
}
