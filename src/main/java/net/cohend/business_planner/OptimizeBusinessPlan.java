package net.cohend.business_planner;

import net.cohend.optimization.PolicyOptimizer;

/**
 * Created by cohend on 4/18/15.
 */
public class OptimizeBusinessPlan {
    public static void main(String[] args) {
        new PolicyOptimizer(new BusinessPolicyClass(), new BusinessObjectives());


        System.out.println("number of business policy evaluations:" + BusinessObjectives.numEvaluations);


    }

}
