package net.cohend.business_planner;

import net.cohend.misc.SimulationPlot;
import net.cohend.modelling.EvaluationObjectives;
import net.cohend.modelling.Policy;
import net.cohend.modelling.Simulator;
import net.cohend.modelling.SimulatorObject;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by cohend on 4/14/15.
 */
public class BusinessObjectives implements EvaluationObjectives {
    public static long numEvaluations = 0;
    /*
    * Two business objectives considered: balance - investment at month 6, numUsers at month 6
    * */
    @Override
    public int numObjectives() {
        return 2;
    }

    @Override
    public double[] evaluatePolicy(Policy policy, boolean verbose, boolean plot) {
        numEvaluations ++;
        Map<String, SimulatorObject> worldModel = new HashMap<>();
        worldModel.put("business", new BusinessSimObject(BusinessParameters.initialInvestment));
        Simulator simulator = new Simulator(worldModel);

        List<Number> x = new LinkedList<>();
        List<Number> bmiOverTime = new LinkedList<>();
        List<Number> usersOverTime = new LinkedList<>();
        List<Number> adsPurchasedOverTime = new LinkedList<>();

        double balanceMinusInvestment = 0;
        double numUsers = 0;
        // simulate for 6 months
        while (simulator.time < 6){
            simulator.tick(policy);
            BusinessSimObject businessSimObject = (BusinessSimObject) simulator.worldModel.get("business");
            balanceMinusInvestment = businessSimObject.balance - businessSimObject.investment;
            if (businessSimObject.hasDefaulted)
                balanceMinusInvestment = -1;
            numUsers = businessSimObject.numUsers;
            if (verbose) {
                System.out.println("num ads purchased:" + businessSimObject.adsPurchased);
                System.out.println("ad capacity:" + businessSimObject.adCapacity);
                System.out.println("num users:" + businessSimObject.numUsers);
                System.out.println("facebook ad budget:" + businessSimObject.facebookAdBudget);
                System.out.println("google ad budget:" + businessSimObject.googleAdBudget);
                System.out.println("objective:" + balanceMinusInvestment);
            }
            if (plot){
                x.add(simulator.time);
                bmiOverTime.add(balanceMinusInvestment);
                usersOverTime.add(businessSimObject.numUsers);
                adsPurchasedOverTime.add(businessSimObject.adsPurchased);
            }
        }

        if (plot){
            Map<String, ImmutablePair<List<Number>, List<Number>>> businessMetrics = new HashMap<>();
            businessMetrics.put("Balance - Investment", new ImmutablePair<>(x, bmiOverTime));
            businessMetrics.put("Number of Users", new ImmutablePair<>(x, usersOverTime));
            businessMetrics.put("Number of Ads Purchased", new ImmutablePair<>(x, adsPurchasedOverTime));
            SimulationPlot p = new SimulationPlot("Business Plan Simulation", businessMetrics);
        }

        return new double[] {-1.0 * balanceMinusInvestment, -1.0 * numUsers};
    }
}
